#!/bin/bash

URL="https://github.com/JonnyMe/picmt-firmware/raw/04cced17ab21d84ea6145e58f0b12fd67ef640d0/picmt_firmware_files.tar.xz"
FILE="out/dl/$(basename "$URL")"
DIR="${FILE//.tar.xz/}/"

if [ ! -d "$DIR" ]; then
    if [ ! -f "$FILE" ]; then
        wget "$URL" -O "$FILE"
    fi
    mkdir -p "$DIR"
    tar -xf "$FILE" -C "$DIR"
fi

cp "$DIR/modem."* "$TMP"
cp "$DIR/mba."* "$TMP"
cp "$DIR/venus.mdt" "$TMP"
cp "$DIR/venus.b"* "$TMP"
cp "$DIR/wcnss."* "$TMP"
cp "$DIR/WCNSS_qcom_wlan_nv.bin" "$TMP"

FILES=()
for file in "$TMP/"*; do
    FILES+=("$file")
done
