#!/bin/bash

URL="https://bigota.d.miui.com/V12.0.3.0.QEJMIXM/beryllium_global_images_V12.0.3.0.QEJMIXM_20201227.0000.00_10.0_global_f4a9dbd1d3.tgz"
FILE="out/dl/$(basename "$URL")"
DIR="${FILE//.tgz/}/"

if [ ! -d "$DIR" ]; then
    if [ ! -f "$FILE" ]; then
        wget "$URL" -O "$FILE"
    fi
    mkdir -p "$DIR"
    tar -xf "$FILE" -C "$DIR" --strip-components=1
fi

cp "$DIR/images/modem.img" "$TMP/modem.img"
cp "$DIR/images/vendor.img" "$TMP/vendor_sparse.img"
python3 simg2img/simg2img.py "$TMP/vendor_sparse.img" "$TMP/vendor.img"

extract_modem_adsp
extract_modem_board-2
extract_modem_cdsp
extract_modem_mba
extract_modem_modem
extract_modem_venus
extract_modem_wlanmdsp
extract_vendor_a630_zap
extract_vendor_ipa_fws
debugfs "$TMP/vendor.img" -R "dump firmware/tas2559_uCDSP.bin $TMP/tas2559_uCDSP.bin"

FILES=()
for file in "$TMP"/{*.mbn,*.jsn,board-2.bin,tas2559_uCDSP.bin}; do
    FILES+=("$file")
done
