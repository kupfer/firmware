#!/bin/bash

function extract_modem_adsp() {
    mcopy -ni "$TMP/modem.img" ::image/adsp* "$TMP"
    pil-squasher/pil-squasher "$TMP/adsp.mbn" "$TMP/adsp.mdt"
}

function extract_modem_board-2() {
    mcopy -ni "$TMP/modem.img" ::image/bdwlan.{b*,1*} "$TMP"

    JSON="$TMP/tmp.json"
    iter=0
    echo "[" >"${JSON}"
    for file in "$TMP"/bdwlan.*; do
        iter=$((iter + 1))
        [ $iter -ne 1 ] && echo "  }," >>"${JSON}"
        echo "  {" >>"${JSON}"
        echo "          \"data\": \"$file\"," >>"${JSON}"
        if [[ $file == "$TMP/bdwlan.bin" ]]; then
            file_ext="ff"
        else
            file_ext="$(printf '%x\n' "$(basename "${file}" | sed -E 's:^.*\.b?([0-9a-f]*)$:0x\1:')")"
        fi
        echo "          \"names\": [\"bus=snoc,qmi-board-id=${file_ext}\"]" >>"${JSON}"
    done
    echo "  }" >>"${JSON}"
    echo "]" >>"${JSON}"
    python2 qca-swiss-army-knife/tools/scripts/ath10k/ath10k-bdencoder -c "${JSON}" -o "$TMP/board-2.bin"
    rm -rf "$JSON"
}

function extract_modem_cdsp() {
    mcopy -ni "$TMP/modem.img" ::image/cdsp* "$TMP"
    pil-squasher/pil-squasher "$TMP/cdsp.mbn" "$TMP/cdsp.mdt"
}

function extract_modem_mba() {
    mcopy -ni "$TMP/modem.img" ::image/mba.mbn "$TMP"
}

function extract_modem_modem() {
    mcopy -ni "$TMP/modem.img" ::image/modem* "$TMP"
    pil-squasher/pil-squasher "$TMP/modem.mbn" "$TMP/modem.mdt"
}

function extract_modem_venus() {
    mcopy -ni "$TMP/modem.img" ::image/venus.* "$TMP"
    pil-squasher/pil-squasher "$TMP/venus.mbn" "$TMP/venus.mdt"
}

function extract_modem_wlanmdsp() {
    mcopy -ni "$TMP/modem.img" ::image/wlanmdsp.mbn "$TMP"
}

function extract_vendor_a630_zap() {
    for i in $(seq -f "%02g" 0 2); do
        debugfs "$TMP/vendor.img" -R "dump firmware/a630_zap.b$i $TMP/a630_zap.b$i"
    done
    debugfs "$TMP/vendor.img" -R "dump firmware/a630_zap.mdt $TMP/a630_zap.mdt"
    pil-squasher/pil-squasher "$TMP/a630_zap.mbn" "$TMP/a630_zap.mdt"

}

function extract_vendor_ipa_fws() {
    for i in $(seq -f "%02g" 0 4); do
        debugfs "$TMP/vendor.img" -R "dump firmware/ipa_fws.b$i $TMP/ipa_fws.b$i"
    done
    debugfs "$TMP/vendor.img" -R "dump firmware/ipa_fws.mdt $TMP/ipa_fws.mdt"
    pil-squasher/pil-squasher "$TMP/ipa_fws.mbn" "$TMP/ipa_fws.mdt"

}

make -C pil-squasher
