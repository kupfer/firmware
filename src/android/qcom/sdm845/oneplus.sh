#!/bin/bash

URL="https://otafsg-cost-az.coloros.com/OnePlus6/OnePlus6Oxygen_22.Y.77_GLO_0770_2107220200/patch/amazone2/GLO/OnePlus6Oxygen/OnePlus6Oxygen_22.Y.77_GLO_0770_2107220200/OnePlus6Oxygen_22.Y.77_OTA_0770_all_2107220200_eabc4b.zip"
FILE="out/dl/$(basename "$URL")"
DIR="${FILE//.zip/}/"

if [ ! -d "$DIR" ]; then
    if [ ! -f "$FILE" ]; then
        wget "$URL" -O "$FILE"
    fi
    unzip "$FILE" -d "$DIR"
fi

python3 -m venv out/venv
source out/venv/bin/activate
pip3 install -r extract_android_payload/requirements.txt

python3 extract_android_payload/extract_android_payload.py extract -p bluetooth "$DIR/payload.bin" "$TMP"
python3 extract_android_payload/extract_android_payload.py extract -p modem "$DIR/payload.bin" "$TMP"
python3 extract_android_payload/extract_android_payload.py extract -p vendor "$DIR/payload.bin" "$TMP"

extract_modem_adsp
extract_modem_board-2
extract_modem_cdsp
extract_modem_mba
extract_modem_modem
extract_modem_venus
extract_modem_wlanmdsp
extract_vendor_a630_zap
extract_vendor_ipa_fws
mcopy -ni "$TMP/modem.img" ::image/slpi* "$TMP"
pil-squasher/pil-squasher "$TMP/slpi.mbn" "$TMP/slpi.mdt"
mcopy -ni "$TMP/bluetooth.img" ::image/crnv21.bin ::image/crbtfw21.tlv "$TMP"

FILES=()
for file in "$TMP"/{*.mbn,*.jsn,crnv21.bin,crbtfw21.tlv,board-2.bin}; do
    FILES+=("$file")
done
