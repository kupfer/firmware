#!/bin/bash
set -e

OUT=out/TARGET
TMP=out/tmp
DL=out/dl

rm -rf $TMP $OUT
mkdir -p out $DL $TMP $OUT
