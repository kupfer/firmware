# firmware

This contains scripts to extract the firmare for several devices automatically from OTAs.  
Currently supported devices are:
- OnePlus 6/6T
- Xiaomi Pocophone F1

The intention is that distros can download the scripts in the top-level directory and extract the firmware that way.
They still need to put the firmware into the correct directory structure themselves though.

The firmware version is always fixed to the script, so distros should checksum the script and which ensures the firmware version hasn't changed.

Some of the submodules are required for some devices. The distro needs to make those available when packaging.
