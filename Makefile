all: fw-android-qcom-sdm845-oneplus fw-android-qcom-sdm845-xiaomi_beryllium fw-android-qcom-msm8916-bq_paella

fw-%:
	cat src/header.sh > $*.sh
	sed -i "s/TARGET/$*/" $*.sh
	IFS='-' read -r -a array <<< "$*"; \
	p=""; \
	for i in $${array[@]}; do \
		p="$$p/$$i"; \
		if [ -f "./src$$p/common.sh" ]; then \
			cat "./src$$p/common.sh" | sed "s|#\!/bin/bash||" >> $*.sh; \
		fi; \
		if [ -f "./src$$p.sh" ]; then \
			cat "./src$$p.sh" | sed "s|#\!/bin/bash||" >> $*.sh; \
		fi; \
	done
	cat src/footer.sh | sed "s|#\!/bin/bash||" >> $*.sh
	chmod a+x $*.sh
