#!/bin/bash
set -e

OUT=out/android-qcom-sdm845-oneplus
TMP=out/tmp
DL=out/dl

rm -rf $TMP $OUT
mkdir -p out $DL $TMP $OUT


function extract_modem_adsp() {
    mcopy -ni "$TMP/modem.img" ::image/adsp* "$TMP"
    pil-squasher/pil-squasher "$TMP/adsp.mbn" "$TMP/adsp.mdt"
}

function extract_modem_board-2() {
    mcopy -ni "$TMP/modem.img" ::image/bdwlan.{b*,1*} "$TMP"

    JSON="$TMP/tmp.json"
    iter=0
    echo "[" >"${JSON}"
    for file in "$TMP"/bdwlan.*; do
        iter=$((iter + 1))
        [ $iter -ne 1 ] && echo "  }," >>"${JSON}"
        echo "  {" >>"${JSON}"
        echo "          \"data\": \"$file\"," >>"${JSON}"
        if [[ $file == "$TMP/bdwlan.bin" ]]; then
            file_ext="ff"
        else
            file_ext="$(printf '%x\n' "$(basename "${file}" | sed -E 's:^.*\.b?([0-9a-f]*)$:0x\1:')")"
        fi
        echo "          \"names\": [\"bus=snoc,qmi-board-id=${file_ext}\"]" >>"${JSON}"
    done
    echo "  }" >>"${JSON}"
    echo "]" >>"${JSON}"
    python2 qca-swiss-army-knife/tools/scripts/ath10k/ath10k-bdencoder -c "${JSON}" -o "$TMP/board-2.bin"
    rm -rf "$JSON"
}

function extract_modem_cdsp() {
    mcopy -ni "$TMP/modem.img" ::image/cdsp* "$TMP"
    pil-squasher/pil-squasher "$TMP/cdsp.mbn" "$TMP/cdsp.mdt"
}

function extract_modem_mba() {
    mcopy -ni "$TMP/modem.img" ::image/mba.mbn "$TMP"
}

function extract_modem_modem() {
    mcopy -ni "$TMP/modem.img" ::image/modem* "$TMP"
    pil-squasher/pil-squasher "$TMP/modem.mbn" "$TMP/modem.mdt"
}

function extract_modem_venus() {
    mcopy -ni "$TMP/modem.img" ::image/venus.* "$TMP"
    pil-squasher/pil-squasher "$TMP/venus.mbn" "$TMP/venus.mdt"
}

function extract_modem_wlanmdsp() {
    mcopy -ni "$TMP/modem.img" ::image/wlanmdsp.mbn "$TMP"
}

function extract_vendor_a630_zap() {
    for i in $(seq -f "%02g" 0 2); do
        debugfs "$TMP/vendor.img" -R "dump firmware/a630_zap.b$i $TMP/a630_zap.b$i"
    done
    debugfs "$TMP/vendor.img" -R "dump firmware/a630_zap.mdt $TMP/a630_zap.mdt"
    pil-squasher/pil-squasher "$TMP/a630_zap.mbn" "$TMP/a630_zap.mdt"

}

function extract_vendor_ipa_fws() {
    for i in $(seq -f "%02g" 0 4); do
        debugfs "$TMP/vendor.img" -R "dump firmware/ipa_fws.b$i $TMP/ipa_fws.b$i"
    done
    debugfs "$TMP/vendor.img" -R "dump firmware/ipa_fws.mdt $TMP/ipa_fws.mdt"
    pil-squasher/pil-squasher "$TMP/ipa_fws.mbn" "$TMP/ipa_fws.mdt"

}

make -C pil-squasher


URL="https://otafsg-cost-az.coloros.com/OnePlus6/OnePlus6Oxygen_22.Y.77_GLO_0770_2107220200/patch/amazone2/GLO/OnePlus6Oxygen/OnePlus6Oxygen_22.Y.77_GLO_0770_2107220200/OnePlus6Oxygen_22.Y.77_OTA_0770_all_2107220200_eabc4b.zip"
FILE="out/dl/$(basename "$URL")"
DIR="${FILE//.zip/}/"

if [ ! -d "$DIR" ]; then
    if [ ! -f "$FILE" ]; then
        wget "$URL" -O "$FILE"
    fi
    unzip "$FILE" -d "$DIR"
fi

python3 -m venv out/venv
source out/venv/bin/activate
pip3 install -r extract_android_payload/requirements.txt

python3 extract_android_payload/extract_android_payload.py extract -p bluetooth "$DIR/payload.bin" "$TMP"
python3 extract_android_payload/extract_android_payload.py extract -p modem "$DIR/payload.bin" "$TMP"
python3 extract_android_payload/extract_android_payload.py extract -p vendor "$DIR/payload.bin" "$TMP"

extract_modem_adsp
extract_modem_board-2
extract_modem_cdsp
extract_modem_mba
extract_modem_modem
extract_modem_venus
extract_modem_wlanmdsp
extract_vendor_a630_zap
extract_vendor_ipa_fws
mcopy -ni "$TMP/modem.img" ::image/slpi* "$TMP"
pil-squasher/pil-squasher "$TMP/slpi.mbn" "$TMP/slpi.mdt"
mcopy -ni "$TMP/bluetooth.img" ::image/crnv21.bin ::image/crbtfw21.tlv "$TMP"

FILES=()
for file in "$TMP"/{*.mbn,*.jsn,crnv21.bin,crbtfw21.tlv,board-2.bin}; do
    FILES+=("$file")
done


for file in "${FILES[@]}"; do
    cp "$file" "$OUT"
done
